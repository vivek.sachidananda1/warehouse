package warehouse

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_calculateStock(t *testing.T) {
	product1 := Product{
		Name:     "Product 1",
		Articles: ArticlesQuantity{{ArticleID: 1, Quantity: 2}},
	}
	product2 := Product{
		Name:     "Product 2",
		Articles: ArticlesQuantity{{ArticleID: 1, Quantity: 2}, {ArticleID: 2, Quantity: 3}},
	}
	product3 := Product{
		Name:     "Product 3",
		Articles: ArticlesQuantity{{ArticleID: 3, Quantity: 30}},
	}
	product0Article := Product{
		Name:     "Product 1",
		Articles: ArticlesQuantity{{ArticleID: 1, Quantity: 0}},
	}

	article1 := Article{ArticleID: 1, Stock: 10}
	article2 := Article{ArticleID: 2, Stock: 10}
	article3 := Article{ArticleID: 3, Stock: 10}

	tests := map[string]struct {
		pList []Product
		aList []Article
		res   []ProductStock
	}{
		"empty": {
			pList: []Product{},
			aList: []Article{},
			res:   []ProductStock{},
		},
		"no articles": {
			pList: []Product{product1, product2, product3},
			aList: []Article{},
			res: []ProductStock{
				{Info: product1, Stock: 0},
				{Info: product2, Stock: 0},
				{Info: product3, Stock: 0},
			},
		},
		"0 availability": {
			pList: []Product{product1},
			aList: []Article{{ArticleID: 1, Stock: 0}},
			res: []ProductStock{
				{Info: product1, Stock: 0},
			},
		},
		"needs 0": {
			pList: []Product{product0Article},
			aList: []Article{article1},
			res: []ProductStock{
				{Info: product0Article, Stock: 0},
			},
		},
		"reported stock": {
			pList: []Product{product1, product2, product3},
			aList: []Article{article1, article2, article3},
			res: []ProductStock{
				{Info: product1, Stock: 5},
				{Info: product2, Stock: 3},
				{Info: product3, Stock: 0},
			},
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			res := calculateStock(test.pList, test.aList)
			assert.Equal(t, test.res, res)

		})
	}
}

func Test_uniqueArticleIds(t *testing.T) {
	product1 := Product{
		Name:     "Product 1",
		Articles: ArticlesQuantity{{ArticleID: 1, Quantity: 2}, {ArticleID: 4, Quantity: 10}},
	}
	product2 := Product{
		Name:     "Product 2",
		Articles: ArticlesQuantity{{ArticleID: 1, Quantity: 2}, {ArticleID: 2, Quantity: 3}},
	}
	product3 := Product{
		Name:     "Product 3",
		Articles: ArticlesQuantity{{ArticleID: 3, Quantity: 30}},
	}
	product4 := Product{
		Name:     "Product 4",
		Articles: ArticlesQuantity{},
	}
	product5 := Product{
		Name: "Product 5",
	}

	res := uniqueArticleIds([]Product{product1, product2, product3, product4, product5})
	assert.Equal(t, []uint{1, 4, 2, 3}, res)

}
