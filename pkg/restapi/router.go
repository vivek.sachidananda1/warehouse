package restapi

func (s *Server) initialize() {
	s.router.GET("/product", s.ListProducts)
	s.router.POST("/product/remove", s.RemoveProduct)
	s.router.POST("/product/add/file", s.AddProducts)
	s.router.POST("/article/add/file", s.AddArticles)
}
