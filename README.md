# Warehouse

## PgAdmin4

PgAdmin4 was used for easy access to the Postgre database during development. For convenience it is part of the deployment.
In order to overwrite the dummy credentials in docker-compose.yml add `/config/pdgadmin/.env` file with the following content:

```text
PGADMIN_DEFAULT_EMAIL=your .email@gmail.com
PGADMIN_DEFAULT_PASSWORD=yourPassword
```


## Running the service

Clone the repository and start the service by running `docker-compose up` it's root folder. This will start:

1. Postgres database - for a purpose of this exercise it is containarized. On the first initial run db and tables are created. In production the DB could be running on managed on Amazon RDS or GCP Cloud SQL
2. Warehouse service - for a purpose of this exercise the service exposes REST Api. It could be easily extended to provide a gRPC interface as well.
3. PgAdmin4 - for easy inspection of the Postgres database. Accesible on `http://localhost:8000`. On a rirst time access it is necessary to [set the server connection](https://www.pgadmin.org/docs/pgadmin4/development/server_dialog.html)
The docker host is `host.docker.internal` for Linux use `172.17.0.1`


## REST Api

| Request| Payload | Method | Response Payload| Description |
| --- | --- | --- | --- | --- |
| /product | - | GET | [Product list](#product-list-response) | Returns OK or InternalServerError |
| /product/delete | [product quantity json](#product-quantity-request) | POST | - | Returns OK, StatusUnprocessableEntity if there is not enough stock, BadRequest, StatusRequestEntityTooLarge (when payload size is exceeded) or InternalServerError|
| /product/add/file | [product list json file](#products-upload-file-format) as multiform | POST | - | Prodct list file needs to be included as a type `file` of a multipart form body with a key `products`. More information about mutipart form request can be found on [w3.org](https://www.w3.org/TR/html401/interact/forms.html#h-17.13.4.2). Returns OK, BadRequest or InternalServerError |
| /article/add/file | [articles list json file](#articles-upload-file-format) as | POST | - | Articles list file needs to be included as a type `file` of a multipart form body with a key `articles`. More information about mutipart form request can be found on [w3.org](https://www.w3.org/TR/html401/interact/forms.html#h-17.13.4.2). Returns OK, BadRequest or InternalServerError |

### Product List Response

```json
{
	"products": [
		{
			"Info": {
				"name": "Product 1",
				"price": 0,
				"contain_articles": [
					{
						"art_id": "1",
						"amount_of": "4"
					}
				]
			},
			"Stock": 1
		},
        {
			"Info": {
				"name": "Product 2",
				"price": 0,
				"contain_articles": [
					{
            "amount_of": "2",
            "art_id": "1"
          },
          {
            "amount_of": "2",
            "art_id": "2"
          }
				]
			},
			"Stock": 1
		}
	]
}
```

### Product Quantity Request

```json
{"name": "Product 1", "quantity": 1}
```

### Products Upload File Format

```json
{
  "products": [
    {
      "contain_articles": [
        {
          "amount_of": "4",
          "art_id": "1"
        }
      ],
      "name": "Product 1"
    },
    {
      "contain_articles": [
        {
          "amount_of": "2",
          "art_id": "1"
        },
        {
          "amount_of": "2",
          "art_id": "2"
        }
      ],
      "name": "Product 2"
    }
  ]
}
```

### Articles Upload File Format

```json
{
  "inventory": [
    {
      "art_id": "1",
      "name": "Article 1",
      "stock": "5"
    },
    {
      "art_id": "2",
      "name": "Article 2",
      "stock": "6"
    }
  ]
}
```

## Considerations for choice of a database

* In warehouse micro service that manages the amount of stock the data is limited
* The warehouse service is not responsible for tracking users, sales or payments only the stock
* There will be more reads than writes
* It is to be expected that there will be very high amount of reads per second
* Product data will be change when new products are added or the price or articles needed changes
* Articles data will change when new artical is added, when stock is added or when product is sold

### Data Caching

 Though not implemented, because the data does not change that oftan comparing to the amount of reads
 it is a good candidate for caching. With each write the affected data would be expired from the cache and
 loaded from the DB with the next read.
 This could be easily extended in the storage package by creating and injecting a cache client alongside the database client.


### Data Structure

The given data structure was not changed, though it is not a good idea to have the Product defined by name.
It would be better to have a UUID the same way as articles have an `article_id`
