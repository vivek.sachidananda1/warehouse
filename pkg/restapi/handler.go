package restapi

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"strings"

	"github.com/julienschmidt/httprouter"

	log "github.com/sirupsen/logrus"
	"gitlab.com/andrea.flogelova/warehouse/pkg/warehouse"
)

type ProductUploadRequest struct {
	Products []warehouse.Product `json:"products"`
}

type ArticleUploadRequest struct {
	Articles []warehouse.Article `json:"inventory"`
}

type ProductListResponse struct {
	Products []warehouse.ProductStock `json:"products"`
}

func (s *Server) ListProducts(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	products, err := s.handler.AllProducts(r.Context())
	log.Infof("Products: %v", products)
	if err != nil {
		log.WithError(err).Errorf("Cannot retrieve products")
		ServeError(http.StatusInternalServerError, "cannot retrieve products", w)
	}

	setHeaders(w)
	if err := json.NewEncoder(w).Encode(ProductListResponse{Products: products}); err != nil {
		log.WithError(err).Errorf("Cannot encode products response")
		ServeError(http.StatusInternalServerError, "cannot encode products response", w)
	}
}

func (s *Server) RemoveProduct(w http.ResponseWriter, r *http.Request, p httprouter.Params) {

	if ct := r.Header.Get("Content-Type"); ct != "" {
		if ct != "application/json" {
			ServeError(http.StatusUnsupportedMediaType, "Content-Type header is not application/json", w)
			return
		}
	}

	r.Body = http.MaxBytesReader(w, r.Body, 1024*1024)
	dec := json.NewDecoder(r.Body)
	dec.DisallowUnknownFields()

	var req warehouse.ProductQuantity
	err := dec.Decode(&req)
	if err != nil {
		var syntaxError *json.SyntaxError
		var unmarshalTypeError *json.UnmarshalTypeError

		switch {

		case errors.As(err, &syntaxError):
			msg := fmt.Sprintf("Request body contains badly-formed JSON (at position %d)", syntaxError.Offset)
			ServeError(http.StatusBadRequest, msg, w)

		case errors.Is(err, io.ErrUnexpectedEOF):
			ServeError(http.StatusBadRequest, "Request body contains badly-formed JSON", w)

		case errors.As(err, &unmarshalTypeError):
			msg := fmt.Sprintf("Request body contains an invalid value for the %q field (at position %d)", unmarshalTypeError.Field, unmarshalTypeError.Offset)
			ServeError(http.StatusBadRequest, msg, w)

		// There is an open issue at https://github.com/golang/go/issues/29035
		// regarding turning this into a sentinel error.
		case strings.HasPrefix(err.Error(), "json: unknown field "):
			ServeError(http.StatusBadRequest, err.Error(), w)

		case errors.Is(err, io.EOF):
			ServeError(http.StatusBadRequest, "Request body must not be empty", w)

		// Unfortunately cannot switch to Go 1.19 yet and use the new http.MaxByteError.
		case err.Error() == "http: request body too large":
			ServeError(http.StatusRequestEntityTooLarge, "Request body must not be larger than 1MB", w)

		default:
			log.WithError(err).Error("Cannot parse request body")
			ServeError(http.StatusInternalServerError, "", w)
		}

		return
	}

	err = s.handler.RemoveProduct(r.Context(), req)
	switch err {
	case warehouse.ErrStockUnavailable:
		ServeError(http.StatusUnprocessableEntity, "insufficient stock", w)
		return
	case nil:
	default:
		log.WithError(err).Error("Cannot remove product")
		ServeError(http.StatusInternalServerError, "", w)
		return
	}

	setHeaders(w)
	w.WriteHeader(http.StatusOK)
}

func (s *Server) AddProducts(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	if err := parseMultiform(r); err != nil {
		ServeError(http.StatusBadRequest, err.Error(), w)
		return
	}

	if r.MultipartForm.File == nil {
		ServeError(http.StatusBadRequest, "multiform has no file", w)
		return
	}

	headers, ok := r.MultipartForm.File["products"]
	if !ok {
		ServeError(http.StatusBadRequest, "multiform has no products file", w)
		return
	}

	products, err := unmarshalProductMultiForm(headers)
	if err != nil {
		log.WithError(err).Errorf("Cannot parse multi form")
		ServeError(http.StatusInternalServerError, "cannot parse multi form", w)
		return
	}

	if err := s.handler.UploadProducts(r.Context(), products); err != nil {
		log.WithError(err).Errorf("Cannot upload products")
		ServeError(http.StatusInternalServerError, "cannot upload products", w)
	}

	setHeaders(w)
	w.WriteHeader(http.StatusOK)
}

func (s *Server) AddArticles(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	if err := parseMultiform(r); err != nil {
		ServeError(http.StatusBadRequest, err.Error(), w)
		return
	}

	if r.MultipartForm.File == nil {
		ServeError(http.StatusBadRequest, "multiform has no file", w)
		return
	}

	headers, ok := r.MultipartForm.File["articles"]
	if !ok {
		ServeError(http.StatusBadRequest, "multiform has no articles file", w)
		return
	}

	articles, err := unmarshalArticleMultiForm(headers)
	if err != nil {
		log.WithError(err).Errorf("Cannot parse multi form")
		ServeError(http.StatusInternalServerError, "cannot parse multi form", w)
		return
	}

	if err := s.handler.UploadArticles(r.Context(), articles); err != nil {
		log.WithError(err).Errorf("Cannot upload articles")
		ServeError(http.StatusInternalServerError, "cannot upload articles", w)
	}

	setHeaders(w)
	w.WriteHeader(http.StatusOK)
}
