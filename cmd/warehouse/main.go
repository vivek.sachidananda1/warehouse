package main

import (
	"context"
	"flag"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/andrea.flogelova/warehouse/pkg/restapi"
	"gitlab.com/andrea.flogelova/warehouse/pkg/storage"
	"gitlab.com/andrea.flogelova/warehouse/pkg/storage/postgres"
	"gitlab.com/andrea.flogelova/warehouse/pkg/warehouse"

	"github.com/kouhin/envflag"
	log "github.com/sirupsen/logrus"
)

func main() {
	pgHost := flag.String("pg-host", "", "Postgres server host")
	pgPort := flag.Uint("pg-port", 5432, "Postgres server port")
	pgUser := flag.String("pg-user", "", "Postgres user")
	pgPass := flag.String("pg-password", "", "Postgres password")
	pgDB := flag.String("pg-database", "", "Postgres database")

	if err := envflag.Parse(); err != nil {
		log.WithError(err).Fatalf("Unable to parse flags")
	}

	log.Infof("warehouse starting")

	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt, syscall.SIGTERM)
	defer cancel()

	// connect to postgres
	pg := postgres.New(postgres.Config{
		User:     *pgUser,
		Password: *pgPass,
		Host:     *pgHost,
		Port:     *pgPort,
		DB:       *pgDB,
	})

	if err := pg.Connect(); err != nil {
		log.Fatalf("cannot connect to postgress: %s", err)
	}

	defer pg.Close()

	// create new warehouse service
	storage, err := storage.New(ctx, &storage.Config{DataProvider: pg})
	if err != nil {
		log.Fatalf("cannot create storage: %s", err)
	}
	cfg := warehouse.Config{
		Storage: storage,
	}
	handler, err := warehouse.NewService(cfg)
	if err != nil {
		log.Fatalf("cannot create new warehouse: %s", err)
	}

	// start new warehouse server rest api
	srv := restapi.NewServer(handler, ":8090")

	if err := srv.Start(); err != nil {
		log.Fatalf("Warehouse server stopped: %s", err)
	}

	defer srv.Stop()

	log.Infof("Terminating application, signal: %v", ctx.Err())
}
