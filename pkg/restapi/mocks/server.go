// Code generated by MockGen. DO NOT EDIT.
// Source: server.go

// Package mocks is a generated GoMock package.
package mocks

import (
	context "context"
	reflect "reflect"

	gomock "github.com/golang/mock/gomock"
	warehouse "gitlab.com/andrea.flogelova/warehouse/pkg/warehouse"
)

// MockHandler is a mock of Handler interface.
type MockHandler struct {
	ctrl     *gomock.Controller
	recorder *MockHandlerMockRecorder
}

// MockHandlerMockRecorder is the mock recorder for MockHandler.
type MockHandlerMockRecorder struct {
	mock *MockHandler
}

// NewMockHandler creates a new mock instance.
func NewMockHandler(ctrl *gomock.Controller) *MockHandler {
	mock := &MockHandler{ctrl: ctrl}
	mock.recorder = &MockHandlerMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockHandler) EXPECT() *MockHandlerMockRecorder {
	return m.recorder
}

// AllProducts mocks base method.
func (m *MockHandler) AllProducts(arg0 context.Context) ([]warehouse.ProductStock, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "AllProducts", arg0)
	ret0, _ := ret[0].([]warehouse.ProductStock)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// AllProducts indicates an expected call of AllProducts.
func (mr *MockHandlerMockRecorder) AllProducts(arg0 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "AllProducts", reflect.TypeOf((*MockHandler)(nil).AllProducts), arg0)
}

// RemoveProduct mocks base method.
func (m *MockHandler) RemoveProduct(arg0 context.Context, arg1 warehouse.ProductQuantity) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "RemoveProduct", arg0, arg1)
	ret0, _ := ret[0].(error)
	return ret0
}

// RemoveProduct indicates an expected call of RemoveProduct.
func (mr *MockHandlerMockRecorder) RemoveProduct(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "RemoveProduct", reflect.TypeOf((*MockHandler)(nil).RemoveProduct), arg0, arg1)
}

// UploadArticles mocks base method.
func (m *MockHandler) UploadArticles(arg0 context.Context, arg1 []warehouse.Article) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "UploadArticles", arg0, arg1)
	ret0, _ := ret[0].(error)
	return ret0
}

// UploadArticles indicates an expected call of UploadArticles.
func (mr *MockHandlerMockRecorder) UploadArticles(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "UploadArticles", reflect.TypeOf((*MockHandler)(nil).UploadArticles), arg0, arg1)
}

// UploadProducts mocks base method.
func (m *MockHandler) UploadProducts(arg0 context.Context, arg1 []warehouse.Product) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "UploadProducts", arg0, arg1)
	ret0, _ := ret[0].(error)
	return ret0
}

// UploadProducts indicates an expected call of UploadProducts.
func (mr *MockHandlerMockRecorder) UploadProducts(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "UploadProducts", reflect.TypeOf((*MockHandler)(nil).UploadProducts), arg0, arg1)
}
