package restapi

//go:generate mockgen -destination=./mocks/$GOFILE -package=mocks -source=$GOFILE

import (
	"context"
	"encoding/json"
	"net/http"
	"time"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/andrea.flogelova/warehouse/pkg/warehouse"

	log "github.com/sirupsen/logrus"
)

type FileType int

const (
	FileTypeProduct FileType = iota
	FileTypeArticles
)

const ParseFormMaxMem = 1024 * 1024

type Handler interface {
	UploadProducts(context.Context, []warehouse.Product) error
	UploadArticles(context.Context, []warehouse.Article) error
	AllProducts(context.Context) ([]warehouse.ProductStock, error)
	RemoveProduct(context.Context, warehouse.ProductQuantity) error
}

// MuxServer contains all components needed for this server
type Server struct {
	url     string
	router  *httprouter.Router
	httpSrv *http.Server
	handler Handler
}

// NewServer creates a new mux server
func NewServer(handler Handler, url string) *Server {
	s := Server{
		url:     url,
		router:  httprouter.New(),
		handler: handler,
	}
	s.initialize()

	return &s
}

// ListenAndServe starts listening for requests
func (s *Server) Start() error {
	log.Infof("Start listening on %s", s.url)
	s.httpSrv = &http.Server{
		Handler:      s.router,
		Addr:         s.url,
		WriteTimeout: 45 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	return s.httpSrv.ListenAndServe()
}

// Stop the server
func (s *Server) Stop() {
	if err := s.httpSrv.Close(); err != nil {
		log.WithError(err).Errorf("Error closing flux simulator server")
	}
}

// ServeError sends a status code to the requester
func ServeError(status int, msg string, w http.ResponseWriter) {
	setHeaders(w)
	w.WriteHeader(status)

	resp := map[string]interface{}{
		"status": http.StatusText(status),
	}

	if msg != "" {
		resp["message"] = msg
	}

	if err := json.NewEncoder(w).Encode(resp); err != nil {
		log.WithError(err).Errorf("error when serving error")
	}
}

func setHeaders(w http.ResponseWriter) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "PUT, POST, GET, OPTIONS")
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Header().Set("Cache-Control", "max-age=0, must-revalidate, no-cache, no-store")
}
