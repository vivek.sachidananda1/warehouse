package warehouse

import "encoding/json"

type ID uint
type Quantity uint

type Article struct {
	ID        ID     `json:"-"`
	ArticleID uint   `json:"art_id,string"`
	Name      string `json:"name"`
	Stock     uint   `json:"stock,string"`
}

type ArticleQuantity struct {
	ArticleID uint `json:"art_id,string"`
	Quantity  uint `json:"amount_of,string"`
}

type ArticlesQuantity []ArticleQuantity

func (a ArticlesQuantity) JSONRawMsg() (json.RawMessage, error) {
	b, err := json.Marshal(a)
	if err != nil {
		return nil, err
	}
	return json.RawMessage(b), nil
}
