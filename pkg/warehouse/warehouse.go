package warehouse

import (
	"context"
	"errors"
	"fmt"

	log "github.com/sirupsen/logrus"
)

var ErrStockUnavailable = errors.New("articles not available in the stock")
var ErrStorageRequired = errors.New("storage is required")

type DataProvider interface {
	AddArticles(context.Context, []Article) error
	Articles(context.Context, []uint) ([]Article, error)
	AddProducts(context.Context, []Product) error
	AllProducts(context.Context) ([]Product, error)
	Product(context.Context, string) (*Product, error)
	RemoveArticles(context.Context, ArticlesQuantity) error
}

type Config struct {
	Storage DataProvider
}

func (c *Config) validate() error {
	if c.Storage == nil {
		return ErrStorageRequired
	}
	return nil
}

func NewService(c Config) (*Service, error) {
	if err := c.validate(); err != nil {
		return nil, err
	}

	return &Service{s: c.Storage}, nil
}

type Service struct {
	s DataProvider
}

func (s *Service) UploadArticles(ctx context.Context, a []Article) error {
	return s.s.AddArticles(ctx, a)
}

func (s *Service) UploadProducts(ctx context.Context, p []Product) error {
	return s.s.AddProducts(ctx, p)
}

func (s *Service) AllProducts(ctx context.Context) ([]ProductStock, error) {
	products, err := s.s.AllProducts(ctx)
	if err != nil {
		return nil, err
	}

	articles, err := s.s.Articles(ctx, uniqueArticleIds(products))
	if err != nil {
		return nil, err
	}

	return calculateStock(products, articles), nil
}

func (s *Service) RemoveProduct(ctx context.Context, q ProductQuantity) error {
	p, err := s.s.Product(ctx, q.Name)
	if err != nil {
		return fmt.Errorf("cannot retrieve product `%s`: %w", q.Name, err)
	}

	return s.s.RemoveArticles(ctx, p.Articles)
}

func calculateStock(pList []Product, aList []Article) []ProductStock {
	articleMap := map[uint]*Article{}
	for _, a := range aList {
		article := a
		articleMap[a.ArticleID] = &article
	}

	stock := []ProductStock{}
	for _, p := range pList {
		product := p
		s := ProductStock{
			Info:  product,
			Stock: countStock(p.Name, p.Articles, articleMap),
		}
		stock = append(stock, s)
	}

	return stock
}

func countStock(productName string, need []ArticleQuantity, inventory map[uint]*Article) uint {
	var productAvailability uint
	var initiated bool
	for _, require := range need {
		// check to avoid division by 0 and log a warning
		if require.Quantity == 0 {
			log.Warnf("Article %d in product `%s`has availabilty 0", require.ArticleID, productName)
			continue
		}

		var articleAvailability uint
		if _, ok := inventory[uint(require.ArticleID)]; ok {
			articleAvailability = inventory[uint(require.ArticleID)].Stock / require.Quantity
		}

		// for the first article we store the availability
		if !initiated {
			initiated = true
			productAvailability = articleAvailability
			continue
		}

		if articleAvailability < productAvailability {
			productAvailability = articleAvailability
		}
	}

	return productAvailability
}

func uniqueArticleIds(products []Product) []uint {
	articlesIDs := []uint{}
	present := map[uint]struct{}{}
	for _, p := range products {
		for _, a := range p.Articles {
			if _, ok := present[a.ArticleID]; !ok {
				present[a.ArticleID] = struct{}{}
				articlesIDs = append(articlesIDs, a.ArticleID)
			}
		}
	}
	return articlesIDs
}
