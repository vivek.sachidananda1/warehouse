package restapi

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"mime"
	"mime/multipart"
	"net/http"

	log "github.com/sirupsen/logrus"
	"gitlab.com/andrea.flogelova/warehouse/pkg/warehouse"
)

func parseMultiform(r *http.Request) error {
	ct := r.Header.Get("Content-Type")
	if ct == "" {
		return errors.New("content type is required")
	}

	mt, _, err := mime.ParseMediaType(ct)
	if err != nil {
		return fmt.Errorf("invalid content type: %s", ct)
	}

	if mt != "multipart/form-data" {
		return fmt.Errorf("unsupported mime type: %s", mt)
	}

	if err := r.ParseMultipartForm(ParseFormMaxMem); err != nil {
		log.WithError(err).Debugf("Can't parse multipart form")
		return errors.New("invalid multipart form")
	}

	return nil
}

func unmarshalProductMultiForm(headers []*multipart.FileHeader) ([]warehouse.Product, error) {
	productsTotal := []warehouse.Product{}

	for _, fileHeader := range headers {
		products := ProductUploadRequest{}
		if err := parseHeader(fileHeader, &products); err != nil {
			return nil, err
		}

		productsTotal = append(productsTotal, products.Products...)
	}

	return productsTotal, nil
}

func unmarshalArticleMultiForm(headers []*multipart.FileHeader) ([]warehouse.Article, error) {
	articlesTotal := []warehouse.Article{}

	for _, fileHeader := range headers {
		articles := ArticleUploadRequest{}
		if err := parseHeader(fileHeader, &articles); err != nil {
			return nil, err
		}

		articlesTotal = append(articlesTotal, articles.Articles...)
	}

	return articlesTotal, nil
}

func parseHeader(h *multipart.FileHeader, into any) error {
	switch into.(type) {
	case *ArticleUploadRequest:
	case *ProductUploadRequest:
	default:
		return errors.New("Not a valid type")
	}

	file, err := h.Open()
	if err != nil {
		return fmt.Errorf("cannot open %s file: %w", h.Filename, err)
	}

	defer file.Close()

	data, err := ioutil.ReadAll(file)
	if err != nil {
		return err
	}

	if err := json.Unmarshal(data, into); err != nil {
		return err
	}

	return nil
}
