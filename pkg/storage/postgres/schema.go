package postgres

import (
	"encoding/json"

	"gitlab.com/andrea.flogelova/warehouse/pkg/warehouse"
)

type Article struct {
	ID        uint   `db:"id"`
	ArticleID uint   `db:"article_id"`
	Name      string `db:"name"`
	Stock     uint   `db:"stock"`
}

type Product struct {
	ID       uint            `db:"id"`
	Name     string          `db:"name"`
	Price    float32         `db:"price"`
	Articles json.RawMessage `db:"articles"`
}

func (p *Product) warehouseFormat() (*warehouse.Product, error) {
	a := warehouse.ArticlesQuantity{}
	if err := json.Unmarshal(p.Articles, &a); err != nil {
		return nil, err
	}

	return &warehouse.Product{
		ID:       warehouse.ID(p.ID),
		Name:     p.Name,
		Price:    p.Price,
		Articles: a,
	}, nil
}

func (a *Article) warehouseFormat() *warehouse.Article {
	return &warehouse.Article{
		ID:        warehouse.ID(a.ID),
		ArticleID: a.ArticleID,
		Name:      a.Name,
		Stock:     a.Stock,
	}
}
