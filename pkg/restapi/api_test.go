package restapi_test

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/andrea.flogelova/warehouse/pkg/restapi"
	"gitlab.com/andrea.flogelova/warehouse/pkg/restapi/mocks"
	"gitlab.com/andrea.flogelova/warehouse/pkg/warehouse"
)

var product1 = warehouse.Product{
	Name:     "Product 1",
	Articles: warehouse.ArticlesQuantity{{ArticleID: 1, Quantity: 2}},
}
var product2 = warehouse.Product{
	Name:     "Product 2",
	Articles: warehouse.ArticlesQuantity{{ArticleID: 1, Quantity: 2}, {ArticleID: 2, Quantity: 3}},
}
var allProductsResponse = []warehouse.ProductStock{{Info: product1, Stock: 5}, {Info: product2, Stock: 12}}
var productQuantity = warehouse.ProductQuantity{Name: "Product 1", Quantity: 5}
var productList = []warehouse.Product{product1, product2}
var articleList = []warehouse.Article{{ArticleID: 1, Name: "Screw", Stock: 5}, {ArticleID: 2, Name: "Leg", Stock: 30}}

func Test_ProductList(t *testing.T) {

	ctrl := gomock.NewController(t)

	// start server with handler that will be returning valid responses on port 1111
	srvOK := defaultServer(ctrl)

	go func(t *testing.T) {
		if err := srvOK.Start(); err != nil {
			log.Infof("Warehouse server stopped: %s", err)
		}
	}(t)

	defer srvOK.Stop()

	// start server with handler that will be return ErrStockUnavailable when removing stock on port 2222
	srvUnavailable := unavailableStockServer(ctrl)

	go func(t *testing.T) {
		if err := srvUnavailable.Start(); err != nil {
			log.Infof("Warehouse unavailable stock server stopped: %s", err)
		}
	}(t)

	defer srvUnavailable.Stop()

	// start server with handler that will be returning error for every call on port 3333
	srvError := errorServer(ctrl)

	go func(t *testing.T) {
		if err := srvError.Start(); err != nil {
			log.Infof("Warehouse error server stopped: %s", err)
		}
	}(t)

	defer srvError.Stop()

	// give some time to the servers to start
	time.Sleep(500 * time.Millisecond)

	type Test struct {
		expectedStatusCode int
		req                func(t *testing.T) (req *http.Request)
		validateResponse   func(t *testing.T, r *http.Response)
	}

	tests := map[string]Test{
		"Product List OK": {
			expectedStatusCode: http.StatusOK,
			req: func(t *testing.T) *http.Request {
				req, err := http.NewRequest("GET", "http://:1111/product", nil)
				if err != nil {
					t.Errorf("Can't create request: %s", err)
				}

				return req
			},
			validateResponse: func(t *testing.T, resp *http.Response) {
				defer resp.Body.Close()
				body, err := ioutil.ReadAll(resp.Body)
				if err != nil {
					t.Fatal("Cannot read response body: ", err)
				}

				var got restapi.ProductListResponse
				if err := json.Unmarshal([]byte(body), &got); err != nil {
					t.Fatal("Cannot unmarshall response body: ", err)
				}
				assert.Equal(t, restapi.ProductListResponse{Products: allProductsResponse}, got)
			},
		},
		"Product Upload List OK": {
			expectedStatusCode: http.StatusOK,
			req: func(t *testing.T) *http.Request {
				toSend := restapi.ProductUploadRequest{Products: productList}
				fileContent, err := json.Marshal(toSend)
				if err != nil {
					t.Fatalf("Can't marshal product upload list: %s", err)
				}

				body, boundary, err := multipartBody("products", fileContent)
				if err != nil {
					t.Errorf("Can't create multipart body: %s", err)
				}

				req, err := http.NewRequest(http.MethodPost, "http://:1111/product/add/file", body)
				if err != nil {
					t.Errorf("Can't create request: %s", err)
				}
				req.Header.Set("Content-Type", fmt.Sprintf("multipart/form-data; boundary=%s", boundary))
				return req
			},
		},
		"Article Upload List OK": {
			expectedStatusCode: http.StatusOK,
			req: func(t *testing.T) *http.Request {
				toSend := restapi.ArticleUploadRequest{Articles: articleList}
				fileContent, err := json.Marshal(toSend)
				if err != nil {
					t.Fatalf("Can't marshal article upload list: %s", err)
				}

				body, boundary, err := multipartBody("articles", fileContent)
				if err != nil {
					t.Errorf("Can't create multipart body: %s", err)
				}

				req, err := http.NewRequest(http.MethodPost, "http://:1111/article/add/file", body)
				if err != nil {
					t.Errorf("Can't create request: %s", err)
				}
				req.Header.Set("Content-Type", fmt.Sprintf("multipart/form-data; boundary=%s", boundary))
				return req
			},
		},
		"Product Remove": {
			expectedStatusCode: http.StatusOK,
			req: func(t *testing.T) *http.Request {
				data, err := json.Marshal(productQuantity)
				if err != nil {
					t.Fatalf("can't marshal product quantity struct")
				}
				reader := bytes.NewReader(data)

				req, err := http.NewRequest(http.MethodPost, "http://:1111/product/remove", reader)
				if err != nil {
					t.Errorf("Can't create request: %s", err)
				}
				return req
			},
		},
		"Product Remove - Unavailable stock": {
			expectedStatusCode: http.StatusUnprocessableEntity,
			req: func(t *testing.T) *http.Request {
				data, err := json.Marshal(productQuantity)
				if err != nil {
					t.Fatalf("can't marshal product quantity struct")
				}
				reader := bytes.NewReader(data)

				req, err := http.NewRequest(http.MethodPost, "http://:2222/product/remove", reader)
				if err != nil {
					t.Errorf("Can't create request: %s", err)
				}
				return req
			},
		},
		"Product Remove - Wrong Input": {
			expectedStatusCode: http.StatusBadRequest,
			req: func(t *testing.T) *http.Request {
				reader := bytes.NewReader([]byte(`{"wrong":"input"}`))

				req, err := http.NewRequest(http.MethodPost, "http://:1111/product/remove", reader)
				if err != nil {
					t.Errorf("Can't create request: %s", err)
				}
				return req
			},
		},
		"Product Upload List - no file": {
			expectedStatusCode: http.StatusBadRequest,
			req: func(t *testing.T) *http.Request {
				body, boundary, err := emptyMultipartBody()
				if err != nil {
					t.Errorf("Can't create multipart body: %s", err)
				}

				req, err := http.NewRequest(http.MethodPost, "http://:1111/product/add/file", body)
				if err != nil {
					t.Errorf("Can't create request: %s", err)
				}
				req.Header.Set("Content-Type", fmt.Sprintf("multipart/form-data; boundary=%s", boundary))
				return req
			},
		},
		"Article Upload List - no file": {
			expectedStatusCode: http.StatusBadRequest,
			req: func(t *testing.T) *http.Request {
				body, boundary, err := emptyMultipartBody()
				if err != nil {
					t.Errorf("Can't create multipart body: %s", err)
				}

				req, err := http.NewRequest(http.MethodPost, "http://:1111/article/add/file", body)
				if err != nil {
					t.Errorf("Can't create request: %s", err)
				}
				req.Header.Set("Content-Type", fmt.Sprintf("multipart/form-data; boundary=%s", boundary))
				return req
			},
		},
		"Product List - Internal Server Error": {
			expectedStatusCode: http.StatusInternalServerError,
			req: func(t *testing.T) *http.Request {
				req, err := http.NewRequest("GET", "http://:3333/product", nil)
				if err != nil {
					t.Errorf("Can't create request: %s", err)
				}

				return req
			},
		},
		"Product Upload List  - Internal Server Error": {
			expectedStatusCode: http.StatusInternalServerError,
			req: func(t *testing.T) *http.Request {
				toSend := restapi.ProductUploadRequest{Products: productList}
				fileContent, err := json.Marshal(toSend)
				if err != nil {
					t.Fatalf("Can't marshal product upload list: %s", err)
				}

				body, boundary, err := multipartBody("products", fileContent)
				if err != nil {
					t.Errorf("Can't create multipart body: %s", err)
				}

				req, err := http.NewRequest(http.MethodPost, "http://:3333/product/add/file", body)
				if err != nil {
					t.Errorf("Can't create request: %s", err)
				}
				req.Header.Set("Content-Type", fmt.Sprintf("multipart/form-data; boundary=%s", boundary))
				return req
			},
		},
		"Article Upload List  - Internal Server Error": {
			expectedStatusCode: http.StatusInternalServerError,
			req: func(t *testing.T) *http.Request {
				toSend := restapi.ArticleUploadRequest{Articles: articleList}
				fileContent, err := json.Marshal(toSend)
				if err != nil {
					t.Fatalf("Can't marshal article upload list: %s", err)
				}

				body, boundary, err := multipartBody("articles", fileContent)
				if err != nil {
					t.Errorf("Can't create multipart body: %s", err)
				}

				req, err := http.NewRequest(http.MethodPost, "http://:3333/article/add/file", body)
				if err != nil {
					t.Errorf("Can't create request: %s", err)
				}
				req.Header.Set("Content-Type", fmt.Sprintf("multipart/form-data; boundary=%s", boundary))
				return req
			},
		},
		"Product Remove - Internal Server Error": {
			expectedStatusCode: http.StatusInternalServerError,
			req: func(t *testing.T) *http.Request {
				data, err := json.Marshal(productQuantity)
				if err != nil {
					t.Fatalf("can't marshal product quantity struct")
				}
				reader := bytes.NewReader(data)

				req, err := http.NewRequest(http.MethodPost, "http://:3333/product/remove", reader)
				if err != nil {
					t.Errorf("Can't create request: %s", err)
				}
				return req
			},
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			client := &http.Client{}
			req := test.req(t)
			resp, err := client.Do(req)
			if err != nil {
				t.Fatal("Cannot do request: ", err)
			}

			assert.Equal(t, test.expectedStatusCode, resp.StatusCode, fmt.Sprintf("%s: expected: %d, got: %d", name, test.expectedStatusCode, resp.StatusCode))
			if test.validateResponse != nil {
				test.validateResponse(t, resp)
			}

		})
	}
}

func defaultServer(ctrl *gomock.Controller) *restapi.Server {
	mockHandlerOK := mocks.NewMockHandler(ctrl)
	mockHandlerOK.EXPECT().AllProducts(gomock.Any()).Return(allProductsResponse, nil).AnyTimes()
	mockHandlerOK.EXPECT().RemoveProduct(gomock.Any(), productQuantity).Return(nil).AnyTimes()
	mockHandlerOK.EXPECT().UploadArticles(gomock.Any(), articleList).Return(nil).AnyTimes()
	mockHandlerOK.EXPECT().UploadProducts(gomock.Any(), productList).Return(nil).AnyTimes()

	return restapi.NewServer(mockHandlerOK, ":1111")
}

func unavailableStockServer(ctrl *gomock.Controller) *restapi.Server {
	mockHandlerUnavailable := mocks.NewMockHandler(ctrl)
	mockHandlerUnavailable.EXPECT().RemoveProduct(gomock.Any(), productQuantity).Return(warehouse.ErrStockUnavailable).AnyTimes()

	return restapi.NewServer(mockHandlerUnavailable, ":2222")
}

func errorServer(ctrl *gomock.Controller) *restapi.Server {
	mockHandlerError := mocks.NewMockHandler(ctrl)
	mockHandlerError.EXPECT().AllProducts(gomock.Any()).Return(nil, errors.New("test error")).AnyTimes()
	mockHandlerError.EXPECT().RemoveProduct(gomock.Any(), productQuantity).Return(errors.New("test error")).AnyTimes()
	mockHandlerError.EXPECT().UploadArticles(gomock.Any(), articleList).Return(errors.New("test error")).AnyTimes()
	mockHandlerError.EXPECT().UploadProducts(gomock.Any(), productList).Return(errors.New("test error")).AnyTimes()

	return restapi.NewServer(mockHandlerError, ":3333")
}

func multipartBody(key string, fileContent []byte) (io.Reader, string, error) {
	body := new(bytes.Buffer)
	writer := multipart.NewWriter(body)

	part1, err := writer.CreateFormFile(key, "info.json")
	if err != nil {
		return nil, "", fmt.Errorf("can't create part 1: %s", err)
	}

	r := bytes.NewReader(fileContent)

	_, err = io.Copy(part1, r)
	if err != nil {
		return nil, "", fmt.Errorf("can't copy part 1: %s", err)
	}

	boundary := writer.Boundary()

	if err := writer.Close(); err != nil {
		return nil, "", fmt.Errorf("can't close writer: %s", err)
	}

	return body, boundary, nil
}

func emptyMultipartBody() (io.Reader, string, error) {
	body := new(bytes.Buffer)
	writer := multipart.NewWriter(body)

	boundary := writer.Boundary()

	if err := writer.Close(); err != nil {
		return nil, "", fmt.Errorf("can't close writer: %s", err)
	}

	return body, boundary, nil
}
