package storage

import (
	"context"
	"errors"

	"gitlab.com/andrea.flogelova/warehouse/pkg/warehouse"
)

type DBClient interface {
	Ping(ctx context.Context) error
	UpsertArticles(context.Context, []warehouse.Article) error
	UpsertProducts(context.Context, []warehouse.Product) error
	Product(context.Context, string) (*warehouse.Product, error)
	AllProducts(context.Context) ([]warehouse.Product, error)
	Articles(context.Context, []uint) ([]warehouse.Article, error)
	RemoveArticles(context.Context, warehouse.ArticlesQuantity) error
}

type Storage struct {
	db DBClient
}

type Config struct {
	DataProvider DBClient
}

func (c *Config) validate(ctx context.Context) error {
	if c.DataProvider == nil {
		return errors.New("data provider is required")
	}

	return c.DataProvider.Ping(ctx)
}

func New(ctx context.Context, c *Config) (*Storage, error) {
	if err := c.validate(ctx); err != nil {
		return nil, err
	}
	return &Storage{db: c.DataProvider}, nil
}

func (s *Storage) AddArticles(ctx context.Context, a []warehouse.Article) error {
	return s.db.UpsertArticles(ctx, a)
}

func (s *Storage) AddProducts(ctx context.Context, p []warehouse.Product) error {
	return s.db.UpsertProducts(ctx, p)
}

func (s *Storage) Articles(ctx context.Context, ids []uint) ([]warehouse.Article, error) {
	return s.db.Articles(ctx, ids)
}

func (s *Storage) AllProducts(ctx context.Context) ([]warehouse.Product, error) {
	return s.db.AllProducts(ctx)
}

func (s *Storage) Product(ctx context.Context, name string) (*warehouse.Product, error) {
	return s.db.Product(ctx, name)
}

func (s *Storage) RemoveArticles(ctx context.Context, a warehouse.ArticlesQuantity) error {
	return s.db.RemoveArticles(ctx, a)
}
