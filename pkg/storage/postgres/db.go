package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"time"

	"github.com/lib/pq"
	_ "github.com/lib/pq"

	log "github.com/sirupsen/logrus"
	"gitlab.com/andrea.flogelova/warehouse/pkg/warehouse"
)

type Config struct {
	User     string
	Password string
	Host     string
	Port     uint
	DB       string
}

type Client struct {
	config Config
	db     *sql.DB
}

func New(c Config) *Client {
	return &Client{
		config: c,
	}
}

func (c *Client) Connect() error {
	connStr := fmt.Sprintf("postgres://%v:%v@%v:%v/%v?sslmode=disable",
		c.config.User,
		c.config.Password,
		c.config.Host,
		c.config.Port,
		c.config.DB)

	var err error
	c.db, err = sql.Open("postgres", connStr)

	return err
}

func (c *Client) Ping(ctx context.Context) error {
	ctx, cancel := context.WithTimeout(ctx, 500*time.Millisecond)
	defer cancel()
	return c.db.PingContext(ctx)
}

func (c *Client) Close() error {
	return c.db.Close()
}

func (c *Client) UpsertProducts(ctx context.Context, products []warehouse.Product) error {
	productNewNames := []string{}
	productsNew := map[string]*warehouse.Product{}
	for _, p := range products {
		product := p
		if _, ok := productsNew[product.Name]; !ok {
			// before starting the transaction prepare a list of new products
			productsNew[product.Name] = &product
			// create a list of new ids for select query
			productNewNames = append(productNewNames, product.Name)
		}
	}

	tx, err := c.db.BeginTx(ctx, nil)
	if err != nil {
		return fmt.Errorf("cannot start transaction: %w", err)
	}

	// Defer a rollback in case anything fails.
	defer func() {
		if err := tx.Rollback(); err != nil {
			log.WithError(err).Warnf("Could not rollback remove article transaction")
		}
	}()

	// Search for articles that already exist
	stmtExPr, err := tx.Prepare("SELECT name, price, articles FROM product WHERE name=ANY($1)")
	if err != nil {
		return fmt.Errorf("cannot prepare statement: %w", err)
	}

	defer stmtExPr.Close()

	rowsExPr, err := stmtExPr.Query(pq.Array(productNewNames))
	if err != nil {
		return fmt.Errorf("query error: %w", err)
	}

	defer rowsExPr.Close()
	updateProduct := []string{}

	for rowsExPr.Next() {
		var p Product
		if err := rowsExPr.Scan(&p.Name, &p.Price, &p.Articles); err != nil {
			return fmt.Errorf("row scan error: %w", err)
		}

		if _, ok := productsNew[p.Name]; ok {
			updateProduct = append(updateProduct, p.Name)
		}
	}

	// update article
	stmtUpdate, err := tx.Prepare("UPDATE product SET price=$1, articles=$2 WHERE name=$3")
	if err != nil {
		return fmt.Errorf("cannot prepare statement: %w", err)
	}

	defer stmtUpdate.Close()

	for _, pName := range updateProduct {
		art, err := productsNew[pName].Articles.JSONRawMsg()
		if err != nil {
			return fmt.Errorf("cannot convert articles to json.RawMessage: %w", err)
		}

		_, err = stmtUpdate.Exec(productsNew[pName].Price, string(art), pName)
		if err != nil {
			return fmt.Errorf("cannot execute statement: %w", err)
		}

		//remove the updated product from the list of new products
		delete(productsNew, pName)
	}

	// create articles
	stmtCreate, err := tx.Prepare("INSERT INTO product (name, price, articles) VALUES ($1, $2, $3)")
	if err != nil {
		return fmt.Errorf("cannot prepare statement: %w", err)
	}

	defer stmtCreate.Close()

	for _, p := range productsNew {
		art, err := p.Articles.JSONRawMsg()
		if err != nil {
			return fmt.Errorf("cannot convert articles to json.RawMessage: %w", err)
		}

		_, err = stmtCreate.Exec(p.Name, p.Price, string(art))
		if err != nil {
			return fmt.Errorf("cannot execute statement: %w", err)
		}
	}

	return tx.Commit()
}

func (c *Client) UpsertArticles(ctx context.Context, articles []warehouse.Article) error {
	articleNewIDs := []uint{}
	articlesNew := map[uint]*warehouse.Article{}
	for _, a := range articles {
		article := a
		if _, ok := articlesNew[article.ArticleID]; !ok {
			// before starting the transaction prepare a list of new articles
			articlesNew[article.ArticleID] = &article
			// create a list of new ids for select query
			articleNewIDs = append(articleNewIDs, article.ArticleID)
		} else {
			if articlesNew[article.ArticleID].Name != article.Name {
				return fmt.Errorf("detected multiple names (%s, %s) for article %d", articlesNew[article.ArticleID].Name, article.Name, article.ArticleID)
			}
			// for repeated articles acumulate the stock amount and ignore the rest of the fields
			// this will result in the use of a name of the first element
			articlesNew[article.ArticleID].Stock += article.Stock
		}
	}

	tx, err := c.db.BeginTx(ctx, nil)
	if err != nil {
		return fmt.Errorf("cannot start transaction: %w", err)
	}

	// Defer a rollback in case anything fails.
	defer func() {
		if err := tx.Rollback(); err != nil {
			log.WithError(err).Warnf("Could not rollback remove article transaction")
		}
	}()

	// Search for articles that already exist
	stmtExSt, err := tx.Prepare("SELECT article_id, stock, name FROM article WHERE article_id=ANY($1)")
	if err != nil {
		return fmt.Errorf("cannot prepare statement: %w", err)
	}

	defer stmtExSt.Close()

	rowsExSt, err := stmtExSt.Query(pq.Array(articleNewIDs))
	if err != nil {
		return fmt.Errorf("query error: %w", err)
	}

	defer rowsExSt.Close()
	updateStock := map[uint]uint{}

	for rowsExSt.Next() {
		var id, stock uint
		var name string
		if err := rowsExSt.Scan(&id, &stock, &name); err != nil {
			return fmt.Errorf("row scan error: %w", err)
		}

		if _, ok := articlesNew[id]; ok {
			if articlesNew[id].Name != name {
				return fmt.Errorf("name of the article to update `%s` differs from existing one `%s` for article %d", articlesNew[id].Name, name, id)
			}
			updateStock[id] = stock
		}
	}

	// update article
	stmtUpdate, err := tx.Prepare("UPDATE article SET name=$1, stock=$2 WHERE article_id=$3")
	if err != nil {
		return fmt.Errorf("cannot prepare statement: %w", err)
	}

	defer stmtUpdate.Close()

	for id, existingStock := range updateStock {
		_, err = stmtUpdate.Exec(articlesNew[id].Name, existingStock+articlesNew[id].Stock, id)
		if err != nil {
			return fmt.Errorf("cannot execute statement: %w", err)
		}

		//remove the updated article from the list of new articles
		delete(articlesNew, id)
	}

	// create articles
	stmtCreate, err := tx.Prepare("INSERT INTO article (article_id, name, stock) VALUES ($1, $2, $3)")
	if err != nil {
		return fmt.Errorf("cannot prepare statement: %w", err)
	}

	defer stmtCreate.Close()

	for _, na := range articlesNew {
		_, err = stmtCreate.Exec(na.ArticleID, na.Name, na.Stock)
		if err != nil {
			return fmt.Errorf("cannot execute statement: %w", err)
		}
	}

	return tx.Commit()
}

func (c *Client) AllProducts(ctx context.Context) ([]warehouse.Product, error) {
	rows, err := c.db.QueryContext(ctx, "SELECT name, price, articles FROM product")
	if err != nil {
		return nil, fmt.Errorf("all products query error: %w", err)
	}

	defer rows.Close()
	products := []warehouse.Product{}

	for rows.Next() {
		p := Product{}
		if err := rows.Scan(&p.Name, &p.Price, &p.Articles); err != nil {
			return nil, fmt.Errorf("all products row scan error: %w", err)
		}

		pw, err := p.warehouseFormat()
		if err != nil {
			return nil, fmt.Errorf("wrong product format: %w", err)
		}
		products = append(products, *pw)
	}

	return products, nil
}

func (c *Client) Articles(ctx context.Context, ids []uint) ([]warehouse.Article, error) {
	stmt, err := c.db.Prepare("SELECT article_id, name, stock FROM article WHERE id=ANY($1)")
	if err != nil {
		return nil, fmt.Errorf("articles prepare query error: %w", err)
	}

	rows, err := stmt.QueryContext(ctx, pq.Array(ids))
	if err != nil {
		return nil, fmt.Errorf("articles query error: %w", err)
	}

	defer rows.Close()
	articles := []warehouse.Article{}

	for rows.Next() {
		a := Article{}
		if err := rows.Scan(&a.ArticleID, &a.Name, &a.Stock); err != nil {
			return nil, fmt.Errorf("articles row scan error: %w", err)
		}

		articles = append(articles, *a.warehouseFormat())
	}

	return articles, nil
}

func (c *Client) Product(ctx context.Context, name string) (*warehouse.Product, error) {
	stmt, err := c.db.Prepare("SELECT name, price, articles FROM product WHERE name=$1")
	if err != nil {
		return nil, fmt.Errorf("product prepare query error: %w", err)
	}
	row := stmt.QueryRowContext(ctx, name)

	p := Product{}

	err = row.Scan(&p.Name, &p.Price, &p.Articles)
	switch err {
	case sql.ErrNoRows:
		return nil, nil
	case nil:
	default:
		return nil, err
	}

	return p.warehouseFormat()
}

func (c *Client) RemoveArticles(ctx context.Context, articles warehouse.ArticlesQuantity) error {
	articleIDs := []uint{}
	stockUpdates := map[uint]uint{}
	for _, a := range articles {
		article := a
		if _, ok := stockUpdates[article.ArticleID]; !ok {
			// before starting the transaction prepare a list of articles with quantities to remove
			stockUpdates[article.ArticleID] = a.Quantity
			// create a list of new ids for select query
			articleIDs = append(articleIDs, article.ArticleID)
		} else {
			// for repeated articles acumulate the stock amount to remove
			stockUpdates[article.ArticleID] += a.Quantity
		}
	}

	tx, err := c.db.BeginTx(ctx, nil)
	if err != nil {
		return fmt.Errorf("cannot start transaction: %w", err)
	}

	// Defer a rollback in case anything fails.
	defer func() {
		if err := tx.Rollback(); err != nil {
			log.WithError(err).Warnf("Could not rollback remove article transaction")
		}
	}()

	// Search for existing stock of articles
	stmtSt, err := tx.Prepare("SELECT article_id, stock FROM article WHERE article_id=ANY($1)")
	if err != nil {
		return fmt.Errorf("cannot prepare statement: %w", err)
	}

	defer stmtSt.Close()

	rowsSt, err := stmtSt.Query(pq.Array(articleIDs))
	if err != nil {
		return fmt.Errorf("query error: %w", err)
	}

	defer rowsSt.Close()
	stockToUpdate := map[uint]uint{}

	for rowsSt.Next() {
		var id, stock uint
		if err := rowsSt.Scan(&id, &stock); err != nil {
			return fmt.Errorf("row scan error: %w", err)
		}

		// exit as soon as one of the articles does not have enough stock
		if stock < stockUpdates[id] {
			return warehouse.ErrStockUnavailable
		}

		stockToUpdate[id] = stock
	}

	// update articles stock
	stmtUpdate, err := tx.Prepare("UPDATE article SET stock=$1 WHERE article_id=$2")
	if err != nil {
		return fmt.Errorf("cannot prepare article stock update statement: %w", err)
	}

	defer stmtUpdate.Close()

	for id, existingStock := range stockToUpdate {
		_, err = stmtUpdate.Exec(existingStock-stockUpdates[id], id)
		if err != nil {
			return fmt.Errorf("cannot execute article stock update statement: %w", err)
		}
	}

	return tx.Commit()
}
