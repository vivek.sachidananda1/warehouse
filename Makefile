PROJECT_NAME := "warehouse"
PKG := "gitlab.com/andrea.flogelova/$(PROJECT_NAME)"
PKG_LIST := $(shell go list ${PKG}/... | grep -v /vendor/)
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go)

.PHONY: build test coverage

test:
	go test -cover -race -timeout 30s ./...

build:
	go build -v ./cmd/warehouse

coverage:
	./tools/coverage.sh;
