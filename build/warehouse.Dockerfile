FROM golang:alpine AS builder
WORKDIR /warehouse
COPY . .

WORKDIR /warehouse/cmd/warehouse
RUN go build -o /bin/warehouse

FROM alpine

EXPOSE 8090

COPY --from=builder /bin/warehouse /warehouse
CMD ["/warehouse"]
