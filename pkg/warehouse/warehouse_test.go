package warehouse_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/andrea.flogelova/warehouse/pkg/warehouse"
)

func Test_NewService(t *testing.T) {
	tests := map[string]struct {
		c   warehouse.Config
		s   *warehouse.Service
		err error
	}{
		"empty config": {
			c:   warehouse.Config{},
			err: warehouse.ErrStorageRequired,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			s, err := warehouse.NewService(test.c)
			assert.Equal(t, test.err, err)
			assert.Equal(t, test.s, s)
		})
	}
}
