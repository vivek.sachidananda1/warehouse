package warehouse

type Product struct {
	ID       ID               `json:"-"`
	Name     string           `json:"name"`
	Price    float32          `json:"price"`
	Articles ArticlesQuantity `json:"contain_articles"`
}

type ProductStock struct {
	Info  Product `json:"info"`
	Stock uint    `json:"stock"`
}

type ProductQuantity struct {
	Name     string `json:"name"`
	Quantity uint   `json:"quantity"`
}
